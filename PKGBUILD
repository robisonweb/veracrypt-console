# Maintainer: Joe Robison <joe@robison.dev>
# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Sebastian Lau <archlinux _at_ slau _dot_ info>
# Contributor: Eric Ozwigh <ozwigh at gmail dot com>

# This requires wxgtk to build even the console version, even
# though there is no need for wx at runtime. We are using source
# instead of just depending b/c Arch does not include static libs
# that (I believe) are required to build console ver.

pkgbase=veracrypt
pkgname=veracrypt-console
_pkgname=VeraCrypt
pkgver=1.25.9
pkgrel=1
pkgdesc='Disk encryption with strong security based on TrueCrypt'
url='https://www.veracrypt.fr/'
arch=('x86_64')
license=('custom:TrueCrypt')
depends=('fuse2>=2.8.0' 'libsm' 'device-mapper')
optdepends=('sudo: mounting encrypted volumes as nonroot users')
makedepends=('yasm')
wxver=3.0.5.1
wxurl="https://github.com/wxWidgets/wxWidgets/releases/download/v${wxver}"
source=(https://launchpad.net/veracrypt/trunk/${pkgver}/+download/VeraCrypt_${pkgver}_Source.tar.bz2{,.sig}
        no-makeself.patch
        "wxWidgets-${wxver}.tar.bz2::${wxurl}/wxWidgets-${wxver}.tar.bz2"
        wxgtk-make-abicheck-non-fatal.patch)
sha512sums=('9b11c8d8e85770ae05960fef8fc9639731e4f9caf0cc4e50bc8c9c92b45d44c80eaeff483d3ab048fd6a82cc873a6027820e21abde7ddb92b3c368f85b837cf2'
            'SKIP'
            '40c269859bb97fbcceb443e5f457788bac650271ed118ec79d34f56fc340ad6e613114fe905ec5aba8c4d171c51c9a6865f97e9fa1ba01fa98ef18be4e97bbe1'
            '0a789fc5e71d414e43f75b5c16076fe8b1bcd7671be0770e4269dcef66d830c1bc74e183f49db270b928862f13472666c283fe2aa98b9006681722e06100725d'
            '5b365d9e0f1e7c9a053514010bd78b4192a4472d6ae76590f6999a4bf04d1de0fae5847fac878ab2dd581f4e0ec3959b317e5efacd3bd6628b89c5f65756cf83')
validpgpkeys=('5069A233D55A0EEB174A5FC3821ACD02680D16DE') # VeraCrypt Team <veracrypt@idrix.fr>

prepare() {
  cd src
  chmod -R u+w . # WAT award
  patch -Np1 < "${srcdir}/no-makeself.patch"

  cd "${srcdir}/wxWidgets-$wxver"
  patch -Np1 -i "${srcdir}/wxgtk-make-abicheck-non-fatal.patch"
}

build() {
  cd "${srcdir}/src"
#  make PKG_CONFIG_PATH=/usr/lib/pkgconfig \
#    TC_EXTRA_LFLAGS+="-ldl ${LDFLAGS}" \
#    TC_EXTRA_CXXFLAGS="${CXXFLAGS} ${CPPFLAGS}" \
#    TC_EXTRA_CFLAGS="${CFLAGS} ${CPPFLAGS}"
#    NOGUI=1 WXSTATIC=1 WX_ROOT=${srcdir}/wxWidgets-${wxver} wxbuild

  make PKG_CONFIG_PATH=/usr/lib/pkgconfig \
    TC_EXTRA_LFLAGS+="-ldl ${LDFLAGS}" \
    TC_EXTRA_CXXFLAGS="${CXXFLAGS} ${CPPFLAGS}" \
    TC_EXTRA_CFLAGS="${CFLAGS} ${CPPFLAGS}" \
    NOGUI=1 WXSTATIC=1 WX_ROOT=${srcdir}/wxWidgets-${wxver} wxbuild

  make PKG_CONFIG_PATH=/usr/lib/pkgconfig \
    TC_EXTRA_LFLAGS+="-ldl ${LDFLAGS}" \
    TC_EXTRA_CXXFLAGS="${CXXFLAGS} ${CPPFLAGS}" \
    TC_EXTRA_CFLAGS="${CFLAGS} ${CPPFLAGS}" \
    NOGUI=1 WXSTATIC=1 WX_ROOT=${srcdir}/wxWidgets-${wxver} 
}

package() {
  cd src
  install -Dm 755 Main/${pkgbase} "${pkgdir}/usr/bin/${pkgname}"
  install -Dm 644 License.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim: ts=2 sw=2 et:
