# VeraCrypt Console Package

This will install VeraCrypt as a console binary, requiring no GUI libraries or
windowing system of any kind to run.

It does, however, require wxWidgets in order to build. I am depending on the
source code of wx. Arch does not include static libs in the official
`extra/wxgtk` package, and I believe they are required even though we are
building a console version which, in the end, does not depend on wx, or even
an X server at all.

Also, I have moved wx to makedepends as I believe building will also need the
X libraries, gtk, etc.

The resulting package will, however, not depend on any X libraries, wxWidgets,
GTK, etc.

## Note on Security

This package is really designed for the *x86* and *x64* platforms. While I have
built, and am currently using without issue, this package on my own ARM boxes
(the Raspberry Pi 3 and soon the ODROID-C2), I cannot guarantee that the build
does not introduce some subtle platform-specific security issue. You have been
warned :)
